package com.eauction.seller.productapp.repository;

import static software.amazon.awssdk.enhanced.dynamodb.mapper.StaticAttributeTags.primaryPartitionKey;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.eauction.seller.productapp.entity.ProductDetail;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.model.PageIterable;

@Repository
@RequiredArgsConstructor
@Log4j2
public class ProductDetailsRepository {
	@Autowired
	private  DynamoDbEnhancedClient dynamoDbEnhancedClient;

    private static final TableSchema<ProductDetail> PRODUCT_DETAILS_TABLE_SCHEMA =
            TableSchema.builder(ProductDetail.class)
                    .newItemSupplier(ProductDetail::new)
                    .addAttribute(String.class, a -> a.name("productId")
                            .getter(ProductDetail::getProductId)
                            .setter(ProductDetail::setProductId)
                            .tags(primaryPartitionKey()))
                    .addAttribute(String.class, a -> a.name("productName")
                            .getter(ProductDetail::getProductName)
                            .setter(ProductDetail::setProductName))
                    .addAttribute(String.class, a -> a.name("shortDescription")
                            .getter(ProductDetail::getShortDescription)
                            .setter(ProductDetail::setShortDescription))
                    .addAttribute(String.class, a -> a.name("detailedDescription")
                            .getter(ProductDetail::getDetailedDescription)
                            .setter(ProductDetail::setDetailedDescription))
                    .addAttribute(String.class, a -> a.name("category")
                            .getter(ProductDetail::getCategory)
                            .setter(ProductDetail::setCategory))
                    .addAttribute(Long.class, a -> a.name("startingPrice")
                            .getter(ProductDetail::getStartingPrice)
                            .setter(ProductDetail::setStartingPrice))
                    .addAttribute(String.class, a -> a.name("bidEndDate")
                            .getter(ProductDetail::getBidEndDate)
                            .setter(ProductDetail::setBidEndDate))
                    .addAttribute(String.class, a -> a.name("firstName")
                            .getter(ProductDetail::getFirstName)
                            .setter(ProductDetail::setFirstName))
                    .addAttribute(String.class, a -> a.name("lastName")
                            .getter(ProductDetail::getLastName)
                            .setter(ProductDetail::setLastName))
                    .addAttribute(String.class, a -> a.name("address")
                            .getter(ProductDetail::getAddress)
                            .setter(ProductDetail::setAddress))
                    .addAttribute(String.class, a -> a.name("city")
                            .getter(ProductDetail::getCity)
                            .setter(ProductDetail::setCity))
                    .addAttribute(String.class, a -> a.name("state")
                            .getter(ProductDetail::getState)
                            .setter(ProductDetail::setState))
                    .addAttribute(String.class, a -> a.name("pin")
                            .getter(ProductDetail::getPin)
                            .setter(ProductDetail::setPin))
                    .addAttribute(Long.class, a -> a.name("phone")
                            .getter(ProductDetail::getPhone)
                            .setter(ProductDetail::setPhone))
                    .addAttribute(String.class, a -> a.name("email")
                            .getter(ProductDetail::getEmail)
                            .setter(ProductDetail::setEmail))
                    .build();


    public void createProduct(ProductDetail productDetail) {
        final DynamoDbTable<ProductDetail> productDetailsTable = dynamoDbEnhancedClient.table("product-details",
                PRODUCT_DETAILS_TABLE_SCHEMA);
        productDetail.setProductId(UUID.randomUUID().toString());
       productDetailsTable.putItem(productDetail);
    }

    public List<ProductDetail> fetchAllProducts() {
        final DynamoDbTable<ProductDetail> productDetailsTable = dynamoDbEnhancedClient.table("product-details",
                PRODUCT_DETAILS_TABLE_SCHEMA);
        // createProductTable();
        //productDetailsTable.putItem(getProductDetail());
        PageIterable<ProductDetail> iterables = productDetailsTable.scan();

//        List<ProductDetail> productDetails = new ArrayList<>();
//        productDetails.add(getProductDetail());
        return iterables.items().stream().collect(Collectors.toList());
        //return  productDetails;
    }

    public ProductDetail fetchProductById(String productId) {
        final DynamoDbTable<ProductDetail> productDetailsTable = dynamoDbEnhancedClient.table("product-details",
                PRODUCT_DETAILS_TABLE_SCHEMA);
        return productDetailsTable.getItem(Key.builder().partitionValue(productId).build());
       // return getProductDetail();
    }

    public void createProductTable() {
//        final DynamoDbTable<ProductDetail> productDetailsTable = dynamoDbEnhancedClient.table("product-details",
//                PRODUCT_DETAILS_TABLE_SCHEMA);
//         productDetailsTable.createTable();
    }

    //Test Data
    private ProductDetail getProductDetail() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setProductId("Test1");
        productDetail.setProductName("Paint Brush");
        productDetail.setCategory("Painting");
        productDetail.setShortDescription("Paint Brush");
        productDetail.setDetailedDescription("It is a branded paint brush");
        productDetail.setBidEndDate("12/12/2022");
        productDetail.setStartingPrice(300);
        productDetail.setFirstName("Bravin");
        productDetail.setLastName("Ninja");
        productDetail.setAddress("S1, Vivekananda Nagar, Sithalapakkam");
        productDetail.setCity("Chennai");
        productDetail.setEmail("bravin@gmail.com");
        productDetail.setPhone(1234567890);
        productDetail.setPin("230000");
        productDetail.setState("TN");
        return productDetail;
    }
}
