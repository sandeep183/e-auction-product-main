package com.eauction.seller.productapp.repository;

import static software.amazon.awssdk.enhanced.dynamodb.mapper.StaticAttributeTags.primaryPartitionKey;
import static software.amazon.awssdk.enhanced.dynamodb.mapper.StaticAttributeTags.primarySortKey;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.eauction.seller.productapp.entity.BuyerBidDetail;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.enhanced.dynamodb.model.PageIterable;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;

@Repository
@RequiredArgsConstructor
@Log4j2
public class BidDetailsRepository {

	@Autowired
    private  DynamoDbEnhancedClient dynamoDbEnhancedClient;

    private static final TableSchema<BuyerBidDetail> BID_DETAIL_TABLE_SCHEMA =
            TableSchema.builder(BuyerBidDetail.class)
                    .newItemSupplier(BuyerBidDetail::new)
                    .addAttribute(String.class, a -> a.name("productId")
                            .getter(BuyerBidDetail::getProductId)
                            .setter(BuyerBidDetail::setProductId)
                            .tags(primaryPartitionKey()))
                    .addAttribute(String.class, a -> a.name("bidAmount")
                            .getter(BuyerBidDetail::getBidAmount)
                            .setter(BuyerBidDetail::setBidAmount))
                    .addAttribute(String.class, a -> a.name("firstName")
                            .getter(BuyerBidDetail::getFirstName)
                            .setter(BuyerBidDetail::setFirstName))
                    .addAttribute(String.class, a -> a.name("lastName")
                            .getter(BuyerBidDetail::getLastName)
                            .setter(BuyerBidDetail::setLastName))
                    .addAttribute(String.class, a -> a.name("address")
                            .getter(BuyerBidDetail::getAddress)
                            .setter(BuyerBidDetail::setAddress))
                    .addAttribute(String.class, a -> a.name("city")
                            .getter(BuyerBidDetail::getCity)
                            .setter(BuyerBidDetail::setCity))
                    .addAttribute(String.class, a -> a.name("state")
                            .getter(BuyerBidDetail::getState)
                            .setter(BuyerBidDetail::setState))
                    .addAttribute(String.class, a -> a.name("pin")
                            .getter(BuyerBidDetail::getPin)
                            .setter(BuyerBidDetail::setPin))
                    .addAttribute(String.class, a -> a.name("phone")
                            .getter(BuyerBidDetail::getPhone)
                            .setter(BuyerBidDetail::setPhone))
                    .addAttribute(String.class, a -> a.name("email")
                            .getter(BuyerBidDetail::getEmail)
                            .setter(BuyerBidDetail::setEmail)
                    .tags(primarySortKey()))
                    .build();


    public List<BuyerBidDetail> fetchBids(String productId) {
        final DynamoDbTable<BuyerBidDetail> bidDetailsTable = dynamoDbEnhancedClient.table("bid-details",
               BID_DETAIL_TABLE_SCHEMA);
       // bidDetailsTable.createTable();
        bidDetailsTable.putItem(getBuyerBidDetail2());
        PageIterable<BuyerBidDetail> iterables = bidDetailsTable.query(QueryConditional.keyEqualTo(Key.builder().partitionValue(productId).build()));
        return iterables.items().stream().collect(Collectors.toList());
    }


    private BuyerBidDetail getBuyerBidDetail1() {
        BuyerBidDetail buyerBidDetail = new BuyerBidDetail();
        buyerBidDetail.setProductId("Test1");
        buyerBidDetail.setBidAmount("3000");
        buyerBidDetail.setFirstName("TestBuyerF1");
        buyerBidDetail.setLastName("TestBuyerL1");
        buyerBidDetail.setEmail("newbuyer@gmail.com");
        buyerBidDetail.setAddress("S3, Buyer Nagar, Bidder");
        buyerBidDetail.setCity("Delhi");
        buyerBidDetail.setPhone("9234567890");
        buyerBidDetail.setPin("400000");
        buyerBidDetail.setState("DL");
        return buyerBidDetail;
    }

    private BuyerBidDetail getBuyerBidDetail2() {
        BuyerBidDetail buyerBidDetail = new BuyerBidDetail();
        buyerBidDetail.setProductId("Test1");
        buyerBidDetail.setBidAmount("4000");
        buyerBidDetail.setFirstName("TestBuyerF2");
        buyerBidDetail.setLastName("TestBuyerL2");
        buyerBidDetail.setEmail("newbuyer2@gmail.com");
        buyerBidDetail.setAddress("P3, Zee Nagar, Bidder");
        buyerBidDetail.setCity("Mumbai");
        buyerBidDetail.setPhone("8234567890");
        buyerBidDetail.setPin("500000");
        buyerBidDetail.setState("MUB");
        return buyerBidDetail;
    }

}