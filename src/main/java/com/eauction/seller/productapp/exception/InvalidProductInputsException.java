package com.eauction.seller.productapp.exception;

public class InvalidProductInputsException extends RuntimeException {
    public InvalidProductInputsException(String message) {
        super(message);
    }
}