package com.eauction.seller.productapp.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.Data;

@Data
@DynamoDBTable(tableName = "product-details")
public class ProductDetail {
    private String productId;
    private String productName;
    private String shortDescription;
    private String detailedDescription;
    private String category;
    private long startingPrice;
    private String bidEndDate;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String pin;
    private long phone;
    private String email;
    @DynamoDBHashKey
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	@DynamoDBAttribute
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	@DynamoDBAttribute
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	@DynamoDBAttribute
	public String getDetailedDescription() {
		return detailedDescription;
	}
	public void setDetailedDescription(String detailedDescription) {
		this.detailedDescription = detailedDescription;
	}
	@DynamoDBAttribute
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@DynamoDBAttribute
	public long getStartingPrice() {
		return startingPrice;
	}
	public void setStartingPrice(long startingPrice) {
		this.startingPrice = startingPrice;
	}
	@DynamoDBAttribute
	public String getBidEndDate() {
		return bidEndDate;
	}
	public void setBidEndDate(String bidEndDate) {
		this.bidEndDate = bidEndDate;
	}
	@DynamoDBAttribute
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@DynamoDBAttribute
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@DynamoDBAttribute
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@DynamoDBAttribute
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@DynamoDBAttribute
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@DynamoDBAttribute
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	@DynamoDBAttribute
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	@DynamoDBAttribute
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}