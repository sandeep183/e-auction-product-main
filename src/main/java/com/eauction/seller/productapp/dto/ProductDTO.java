package com.eauction.seller.productapp.dto;

import lombok.Data;

@Data
public class ProductDTO {
    private String productId;
    private String productName;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
}
