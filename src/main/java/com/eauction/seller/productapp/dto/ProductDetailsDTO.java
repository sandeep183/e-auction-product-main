package com.eauction.seller.productapp.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class ProductDetailsDTO {
    private String productId;
    @NotNull
    @Size(min=5, max=30, message="Product name length should be between 5 to 30 characters")
    private String productName;
    private String shortDescription;
    private String detailedDescription;
    private String category;
    @NotNull
    @PositiveOrZero
    private long startingPrice;
    private String bidEndDate;
    @NotNull
    @Size(min=5, max=30, message="First Name length should be between 5 to 30 characters")
    private String firstName;
    @NotNull
    @Size(min=3, max=30, message="Last Name length should be between 3 to 30 characters")
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String pin;
    @NotNull
    @Digits(integer = 10, fraction = 0)
    private long phone;
    @NotNull
    @Email
    private String email;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public String getDetailedDescription() {
		return detailedDescription;
	}
	public void setDetailedDescription(String detailedDescription) {
		this.detailedDescription = detailedDescription;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public long getStartingPrice() {
		return startingPrice;
	}
	public void setStartingPrice(long startingPrice) {
		this.startingPrice = startingPrice;
	}
	public String getBidEndDate() {
		return bidEndDate;
	}
	public void setBidEndDate(String bidEndDate) {
		this.bidEndDate = bidEndDate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}